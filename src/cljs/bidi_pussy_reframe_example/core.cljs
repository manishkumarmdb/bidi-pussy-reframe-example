(ns bidi-pussy-reframe-example.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [devtools.core :as devtools]
              [bidi-pussy-reframe-example.handlers]
              [bidi-pussy-reframe-example.subs]
              [bidi-pussy-reframe-example.routes :as routes]
              [bidi-pussy-reframe-example.views :as views]
              [bidi-pussy-reframe-example.config :as config]))


(defn dev-setup []
  (when config/debug?
    (println "dev mode")
    (devtools/install!)))

(defn mount-root []
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
